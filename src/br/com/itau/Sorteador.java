package br.com.itau;

import java.util.Random;

public class Sorteador {
    public static int dado() {
        Random rnd = new Random();
        return rnd.nextInt(6) + 1;
    }

    public static int[][] sortearGrupo(int qtdGrupos, int qtdDados) {
        int[][] resultado = new int[qtdGrupos][qtdDados];

        for (int[] grupo : resultado) {

            for (int j=0; j < grupo.length; j++) {
                grupo[j] = Sorteador.dado();
            }

        }

        return resultado;
    }


}
