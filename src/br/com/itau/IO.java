package br.com.itau;

import java.util.Scanner;

public class IO {

    public static void imprimirSorteio(int [][] sorteio) {
        String sorteioFormatado = "";

        for (int[] grupo : sorteio) {

            for (int j=0; j < grupo.length; j++) {
                sorteioFormatado += grupo[j];
                if(j != grupo.length-1){
                    sorteioFormatado += ", ";
                }
            }

            sorteioFormatado += "\n";
        }

        System.out.println(sorteioFormatado);
    }

    public static void imprimirDado(int valor){
        System.out.println(valor+"\n");
    }

}
