package br.com.itau;

public class Resultado {

    public static int[][] calculaSoma(int[][] sorteio) {
        int[][] resultado = new int[sorteio.length][sorteio[0].length+1];

        for(int i=0; i < sorteio.length; i++) {
            int soma = 0;
            for(int j=0; j < sorteio[i].length; j++){
                resultado[i][j] = sorteio[i][j];
                soma += sorteio[i][j];
            }
            resultado[i][sorteio[i].length] = soma;
        }

        return resultado;

    }

}
