package br.com.itau;

public class Main {

    public static void main(String[] args) {
        // Primeira parte do exercício
        IO.imprimirDado(Sorteador.dado());

        // Segunda parte do exercício
        IO.imprimirSorteio(Resultado.calculaSoma(Sorteador.sortearGrupo(1, 3)));

        // Terceira parte do exercício
        IO.imprimirSorteio(Resultado.calculaSoma(Sorteador.sortearGrupo(3, 3)));


    }
}
